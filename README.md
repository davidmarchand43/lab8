Lab 8
David Marchand

1. The reason the logger is different from the console is because of the level indicated in the logger.properties file. 
	It indicates that the console log is to display information of the "INFO" level, while the file logger is to display
	information at the "ALL" level.

2. It comes from the ConditionEvaluator and DisabledCondition classes.  ConditionEvaluator's method "logResult" 
	is called, which contains a variable "ConditionalEvaluationResult result", which contains the information
	about the particular exception.

3. Assertions.assertThrows assert that a particular exception has been thrown. Basically it checks if a program
	threw the exception it was supposed to, when it was expected to.
	
4.1 serialVersionUID is used to determine if a version of a class is compatible with
	serialized data (stored data, basically). If you update a class, its serialVersionUID
	might change, because you altered how it interacted with stored data, making it
	incompatible with that stored data. The serialVersionUID allows Java to make sure
	the data it is accessing is not out of data or incompatible.
	
4.2  Because, by overriding it with a specific message, we can later retrieve that
	message and view it. 

4.3 Because we do not need to change the other methods, by changing the 
	constructors, we gave the class the functionality we needed.
	
5. It starts as soon as the program begins running. It opens the logger properties and reads them,
	using them to configure the logger. Then it outputs a message to the logger.

6. .md is Markdown, "a plain text format for writing structured documents" (https://commonmark.org)
	It used because it is unambiguous, and standardized. It is related to bitbucket because bitbucket
	automatically readme.md files.

7. The program expects a TimerException exception, but is recieving a NullPointerException. 
	This is because of the timeNow variable being null. To fix this, move the 
	"timeNow = System.currentTimeMillis();" above the "if (timeToWait < 0) {" line.
	
8. Even though the method correctly threw a TimerException, the timeNow variable was null, 
	so the most recent exception being throw was NullPointerException. Even Though it caught
	the TimerException, it ran the "finally" block afterwards, which tried accessing
	the value of timeNow, which was null, resulting in the NullPointerException.
	
9. 
	
10.

11. The category of TimerException is an InterruptedException, which is a compile-time exception. 
	NullPointerException is a RuntimeException.